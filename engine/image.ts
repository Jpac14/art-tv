import axios from "axios"
import sharp from "sharp"
import {randomItem} from "./util"
import {Readable} from "stream"

const BASE_URL = "https://collectionapi.metmuseum.org/public/collection/v1"
const COLLECTION_URL = BASE_URL + "/search?q=*&isHighlight=true&hasImage=true"
const OBJECT_URL = BASE_URL + "/objects/"
const BAD_IMAGE_IDS = [282774, 310453, 312342, 352328, 159388, 499720, 749639, 485416, 488221]

class Image {
  imageIds: number[]

  async loadImages(): Promise<void> {
    const res = await axios.get(COLLECTION_URL)
    this.imageIds = res.data.objectIDs
    this.imageIds = this.imageIds.filter((val) => !BAD_IMAGE_IDS.includes(val))
  }

  async getRandomImageScaled(): Promise<Readable> {
    const url = await this.getRandomImageInfo()
    const image = await this.downloadImage(url)
    const scaled = await this.rescaleImage(image)

    return scaled
  }
  
  async rescaleImage(stream: Readable): Promise<Readable> {
    const transformer = sharp().resize({width: 3840, height: 2160})
    return stream.pipe(transformer)
  }

  async getRandomImageInfo(): Promise<string> {
    const id = randomItem(this.imageIds)
    const res = await axios.get(OBJECT_URL + id)
    return res.data.primaryImage
  }

  async downloadImage(url: string): Promise<Readable> {
    const res = await axios.get(url, {responseType: "stream"})
    return res.data
  }
}

export {Image}
