import {execFile, spawn, ChildProcess} from "child_process"
import {Readable} from "stream"
import {Image} from "./image"

class Queue {
  image: Image
  hlsChild: ChildProcess

  constructor() {
    this.image = new Image()
    this.hlsChild = execFile("ffmpeg", [
      "-y",
      "-f",
      "mpegts",
      "-re",
      "-i",
      "pipe:",
      "-acodec",
      "copy",
      "-vcodec",
      "copy",
      "-f",
      "hls",
      "-hls_flags",
      "delete_segments",
      "-hls_list_size",
      "4",
      "-hls_time",
      "1",
      "hls/master.m3u8",
    ])
  }

  async load(): Promise<void> {
    await this.image.loadImages()
  }

  // Figure out to encode to h.264 for 4K or even 8K on Samsung TV in real-time
  // Or find codec Samsung supports for 4K
  // https://developer.samsung.com/smarttv/develop/specifications/media-specifications/2017-tv-video-specifications.html
  // Find fast way to encode to video formats like h.264, h.265, VP9 and others, can't get playing realtime.
  // Maybe generate 1 second clip them loop?
  // Gstreamer is another option
  async imageToVideo(stream: Readable) {
    const mpegChild = spawn("ffmpeg", [
      "-r",
      "1/30",
      "-f",
      "jpeg_pipe",
      "-re",
      "-i",
      "pipe:0",
      "-c:v",
      "mpeg2video",
      "-qscale:v",
      "2",
      "-vf",
      "fps=25",
      "-pix_fmt",
      "yuv420p",
      "-f",
      "mpegts",
      "pipe:1",
    ])

    stream.pipe(mpegChild.stdin)
    return mpegChild.stdout
  }

  async generateVideo(): Promise<Readable> {
    const image = await this.image.getRandomImageScaled()
    const video = await this.imageToVideo(image)
    return video
  }

  send(chunk: any) {
    this.hlsChild.stdin?.write(chunk)
  }

  async play() {
    const stream = await this.generateVideo()

    stream
      .on("data", (chunk) => this.send(chunk))
      .on("error", () => this.play())
      .on("end", () => this.play())
  }
}

export {Queue}
