function randomItem<Type>(array: Type[]): Type {
  return array[Math.floor(Math.random() * array.length)]
}

export {randomItem}
