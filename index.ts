import {Queue} from "./engine/queue"

// Clean up code, rename functions
// Implement queue system with workers, maybe?
// Preprocess image to video, before it is played.
// Better error handling
// Logging
// Configuration
// Samsung TV app with avplay
// File hosting with nginx
// OSS stuff (docker)
// Make into paid service

(async () => {
  const queue = new Queue()

  await queue.load()
  await queue.play()
})()
